package model;

import io.micrometer.common.util.StringUtils;

/**
 * Class to represent CryptoCurrency as an entity with attributes
 */
public class CryptoCurrency {
    private String cryptoCurrencyName;
    private Double marketCap;
    private Double volume;
    private Double currentPrice;

    private String month;

    public String getCryptoCurrencyName() {
        return cryptoCurrencyName;
    }

    public void setCryptoCurrencyName(String cryptoCurrencyName) {
        this.cryptoCurrencyName = cryptoCurrencyName;
    }

    public Double getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(Double marketCap) {
        this.marketCap = marketCap;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public CryptoCurrency(String cryptoCurrencyName, Double marketCap, Double volume, Double currentPrice, String month) {
        this.cryptoCurrencyName = cryptoCurrencyName;
        this.marketCap = marketCap;
        this.volume = volume;
        this.currentPrice = currentPrice;
        this.month = month;
    }

    public CryptoCurrency() {
    }

    /**
     * Method to return price range for a month
     * @param month
     * @param cryptoCurrency
     * @return
     */
    public static Double getPriceRangesForTimePeriod(String month, CryptoCurrency cryptoCurrency) {
        assert StringUtils.isEmpty(month);
        if (cryptoCurrency.getMonth().equalsIgnoreCase(month)) {
            return cryptoCurrency.getCurrentPrice();
        }
        return 0.0;
    }
}