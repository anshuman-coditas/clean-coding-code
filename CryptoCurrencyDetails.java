import model.CryptoCurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Scanner;

public class CryptoCurrencyDetails {

    private static final String CRYPTO_API = "https://www.crypto-api.org";
    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoCurrencyDetails.class);


    public static void main(String[] args) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            LOGGER.info("Fetching CryptoCurrency Details --> ");
            CryptoCurrency cryptoCurrency = restTemplate.getForObject(CRYPTO_API, CryptoCurrency.class);
//            Checking if API returned a null object
            assert cryptoCurrency != null;
            LOGGER.info("CryptoCurrency Fetched : {}", cryptoCurrency);

            Scanner scanner = new Scanner(System.in);
            LOGGER.info("Taking month as an input");
            String month = scanner.nextLine();

            LOGGER.info("Fetching Price Range for month {}", month);
            Double priceRangeCryptoCurrency = CryptoCurrency.getPriceRangesForTimePeriod(month, cryptoCurrency);
            LOGGER.info("Price range is : {}", priceRangeCryptoCurrency);

//            Validating Name for the fetched CryptoCurrency
            if (cryptoCurrency.getCryptoCurrencyName().contains("@")) {
                LOGGER.error("Invalid Name");
                throw new IllegalArgumentException("Invalid Currency Name");
            }

        } catch (IllegalArgumentException e) {
            LOGGER.error("Invalid Name Provided");
            System.out.println(e.getMessage() + "Error Code: ");

        } catch (RestClientException e) {
            LOGGER.error("RestTemplate Client reused to respond");
            System.out.println("RestTemplate refused to response");
        }

    }


}
